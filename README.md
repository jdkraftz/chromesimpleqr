About
-----
A simple chrome extension I am mainly developing to get some practice git both with Github and Bitbucket.

Features
--------
Currently all it does is generate a QR code for the URL of the current tab. 

It uses the [QRCode.js library](https://github.com/davidshimjs/qrcodejs) for generating QR codes.